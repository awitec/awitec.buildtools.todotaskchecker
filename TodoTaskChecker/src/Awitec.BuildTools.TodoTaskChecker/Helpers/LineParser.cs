﻿using System.Text.RegularExpressions;
using Awitec.BuildTools.TodoTaskChecker.Model;

namespace Awitec.BuildTools.TodoTaskChecker.Helpers
{
    internal class LineParser : ILineParser
    {
        public LineInfo ParseLine(string line)
        {
            if (string.IsNullOrWhiteSpace(line))
            {
                return LineInfo.Default;
            }

            var todoMatch = Regex.Match(line, @"\bTODO\b", RegexOptions.IgnoreCase);

            if (!todoMatch.Success)
            {
                return LineInfo.Default;
            }

            var taskIdMatch = Regex.Match(line.Substring(todoMatch.Index, line.Length - todoMatch.Index), "[1-9][0-9]*");

            return new LineInfo(true, taskIdMatch.Success ? taskIdMatch.Value : string.Empty);
        }
    }
}
