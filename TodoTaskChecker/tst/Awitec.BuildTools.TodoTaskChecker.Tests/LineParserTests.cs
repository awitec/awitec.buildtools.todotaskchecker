﻿using Awitec.BuildTools.TodoTaskChecker.Helpers;
using Shouldly;
using Xunit;

namespace Awitec.BuildTools.TodoTaskChecker.Tests
{
    public class LineParserTests
    {

        [Theory]
        [InlineData("")]
        [InlineData("\t")]
        [InlineData(null)]
        public void ParseLine_LineIsNullEmptyWhiteSpace_ReturnsFalse(string line)
        {
            var lineParser = new LineParser();

            var info = lineParser.ParseLine(line);

            info.ShouldNotBeNull();
            info.ContainsTodo.ShouldBeFalse();
        }

        [Theory]
        [InlineData("// TODO do something")]
        [InlineData("// TODO: do something")]
        [InlineData("TODO do something")]
        [InlineData("TODO: do something")]
        public void ParseLine_LineIsGivenContainsTodo_ReturnsTrue(string line)
        {
            var lineParser = new LineParser();

            var info = lineParser.ParseLine(line);

            info.ShouldNotBeNull();
            info.ContainsTodo.ShouldBeTrue();
        }

        [Theory]
        [InlineData("TODO 1234 do something", "1234")]
        [InlineData("TODO: 1234 do something", "1234")]
        [InlineData("TODO do something 99", "99")]
        [InlineData("TODO: TASK123445", "123445")]
        public void ParseLine_LineIsGivenContainsTaskNumber_TaskNumberIsRecognized(string line, string taskNumber)
        {
            var lineParser = new LineParser();

            var info = lineParser.ParseLine(line);

            info.ShouldNotBeNull();
            info.ContainsTodo.ShouldBeTrue();
            info.TaskId.ShouldBe(taskNumber);
        }

        [Theory]
        [InlineData("TODO do something")]
        [InlineData("TODO: do something")]
        public void ParseLine_LineIsGivenNoTaskNumber_TaskNumberIsEmpty(string line)
        {
            var lineParser = new LineParser();

            var info = lineParser.ParseLine(line);

            info.ShouldNotBeNull();
            info.ContainsTodo.ShouldBeTrue();
            info.TaskId.ShouldBeEmpty();
        }

        [Theory]
        [InlineData("there is no task to do")]
        [InlineData("even-though there is a number 1234, task number is empty")]
        public void ParseLine_LineIsGivenNoToDo_TaskNumberIsEmpty(string line)
        {
            var lineParser = new LineParser();

            var info = lineParser.ParseLine(line);

            info.ShouldNotBeNull();
            info.ContainsTodo.ShouldBeFalse();
            info.TaskId.ShouldBeEmpty();
        }
    }
}
