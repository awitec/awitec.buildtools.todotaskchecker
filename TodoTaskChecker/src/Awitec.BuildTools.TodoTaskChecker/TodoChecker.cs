﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Awitec.BuildTools.TodoTaskChecker.Helpers;
using Awitec.BuildTools.TodoTaskChecker.Model;

[assembly: InternalsVisibleTo("Awitec.BuildTools.TodoTaskChecker.Tests")]

namespace Awitec.BuildTools.TodoTaskChecker
{
    public class TodoChecker
    {
        private const string SourceFileExtension = "cs";

        private readonly IFilesFinder _fileFinder;
        private readonly ITasksFinder _tasksFinder;
        private readonly IToDosFinder _toDosFinder;

        internal TodoChecker(IFilesFinder fileFinder, ITasksFinder tasksFinder, IToDosFinder toDosFinder)
        {
            _fileFinder = fileFinder ?? throw new ArgumentNullException(nameof(fileFinder));
            _tasksFinder = tasksFinder ?? throw new ArgumentNullException(nameof(tasksFinder));
            _toDosFinder = toDosFinder ?? throw new ArgumentNullException(nameof(toDosFinder));
        }

        public static TodoChecker Create()
        {
            return new TodoChecker(new FilesFinder(), new TasksFinder(), new ToDosFinder(new LineParser()));
        }

        public bool DoAllToDosHaveOpenTask(string serverUrl, string projectName, string accessToken, string workingDirectory)
        {
            if (string.IsNullOrWhiteSpace(serverUrl)) throw new ArgumentNullException(nameof(serverUrl));
            if (string.IsNullOrWhiteSpace(projectName)) throw new ArgumentNullException(nameof(projectName));
            if (string.IsNullOrWhiteSpace(accessToken)) throw new ArgumentNullException(nameof(accessToken));
            if (string.IsNullOrWhiteSpace(workingDirectory)) throw new ArgumentNullException(nameof(workingDirectory));

            Console.Write("Retrieving non-closed tasks from the server ... ");

            var allOpenTasks = _tasksFinder.GetWorkItems(projectName, serverUrl, accessToken).ToArray();

            Console.WriteLine($"{allOpenTasks.Length} tasks returned.");

            var nonTrackedToDos = new List<ToDo>();

            Console.Write("Scanning all source files in the working directory ... ");

            var sourceFiles = _fileFinder.SearchForFiles(workingDirectory, SourceFileExtension).ToArray();

            foreach (var sourceFile in sourceFiles)
            {
                foreach (var todo in _toDosFinder.SearchForToDos(sourceFile))
                {
                    if (string.IsNullOrWhiteSpace(todo.TaskNumber) || !allOpenTasks.Any(t => t.Id.Equals(todo.TaskNumber)))
                    {
                        nonTrackedToDos.Add(todo);
                    }
                }
            }

            Console.WriteLine($"{sourceFiles.Length} files processed.");

            OutputNonTrackedTasks(nonTrackedToDos);

            return !nonTrackedToDos.Any();
        }

        private static void OutputNonTrackedTasks(IList<ToDo> nonTrackedToDos)
        {
            if (!nonTrackedToDos.Any())
            {
                Console.WriteLine("All ToDos have corresponding non-closed task.");
                return;
            }

            Console.WriteLine("ToDos without task:");

            foreach (var todo in nonTrackedToDos)
            {
                Console.WriteLine($"{todo.FileName} - {todo.LineNumber}: {todo.LineContent}");
            }
        }
    }
}
