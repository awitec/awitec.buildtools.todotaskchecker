﻿using System;
using System.IO;
using System.Collections.Generic;
using Awitec.BuildTools.TodoTaskChecker.Model;

namespace Awitec.BuildTools.TodoTaskChecker.Helpers
{
    internal class ToDosFinder : IToDosFinder
    {
        private readonly ILineParser _lineParser;

        public ToDosFinder(ILineParser lineParser)
        {
            _lineParser = lineParser ?? throw new ArgumentNullException(nameof(lineParser));
        }

        public IEnumerable<ToDo> SearchForToDos(SourceFile file)
        {
            var l = 0;

            using (var sr = new StreamReader(file.FullPath))
            {
                while (!sr.EndOfStream)
                {
                    ++l;
                    var line = sr.ReadLine();
                    var lineResult = _lineParser.ParseLine(line);

                    if (lineResult.ContainsTodo)
                    {
                        yield return new ToDo
                        {
                            LineNumber = l,
                            FileName = file.FullName,
                            TaskNumber = lineResult.TaskId,
                            LineContent = line
                        };
                    }
                }
            }
        }
    }
}
