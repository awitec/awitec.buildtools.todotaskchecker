﻿using CommandLine;

namespace Awitec.BuildTools.TodoTaskChecker.Console
{
    internal class Arguments
    {
        [Option('s', Required = true, HelpText = "AzureDevOps server URl")]
        public string ServerUrl { get; set; }

        [Option('p', Required = true, HelpText = "AzureDevOps project name")]
        public string ProjectName { get; set; }

        [Option('t', Required = true, HelpText = "AzureDevOps access token")]
        public string AccessToken { get; set; }

        [Option('d', Required = true, HelpText = "Working directory to search in")]
        public string WorkingDirectory { get; set; }
    }
}
