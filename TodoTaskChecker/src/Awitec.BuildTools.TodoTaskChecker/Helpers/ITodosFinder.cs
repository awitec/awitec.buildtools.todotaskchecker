﻿using System.Collections.Generic;
using Awitec.BuildTools.TodoTaskChecker.Model;

namespace Awitec.BuildTools.TodoTaskChecker.Helpers
{
    public interface IToDosFinder
    {
        IEnumerable<ToDo> SearchForToDos(SourceFile file);
    }
}