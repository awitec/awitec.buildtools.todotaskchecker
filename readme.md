# ToDoTaskChecker

ToDoTaskChcker is a little app supposed to be hooked-up into the CI/CD pipeline. The objective is to collect all TODOs in the C# source files and verify if they refer to the non-closed work item. If such TODOs are detected, they are listed in the output and program exits with -1 exit code, otherwise with 0.

Bash example:

```sh
$ TodoTaskChecker -s https://fabricam.visualstudio.com -p MyProject -t ABCDEF123 -d ~/documents/mysourcedirectory
```

Where arguments are:

| Argument | Meaning |
| ------ | ------ |
| -s | your VSTS server URL |
| -p | project name |
| -t | access token |
| -d | directory containing sources to check |