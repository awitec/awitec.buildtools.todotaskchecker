﻿using CommandLine;

namespace Awitec.BuildTools.TodoTaskChecker.Console
{
    class Program
    {
        public static int Main(string[] args)
        {
            System.Console.WriteLine("Checking if all TODOs have assigned non-closed Task");

            var allToDosHaveOpenTask = false;

            Parser.Default.ParseArguments<Arguments>(args).WithParsed(a =>
            {
                allToDosHaveOpenTask = TodoChecker
                    .Create()
                    .DoAllToDosHaveOpenTask(a.ServerUrl, a.ProjectName, a.AccessToken, a.WorkingDirectory);
            });

            return allToDosHaveOpenTask ? 0 : -1;
        }
    }
}
