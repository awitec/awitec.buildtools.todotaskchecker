﻿namespace Awitec.BuildTools.TodoTaskChecker.Model
{
    public class WorkTask
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }
    }
}
