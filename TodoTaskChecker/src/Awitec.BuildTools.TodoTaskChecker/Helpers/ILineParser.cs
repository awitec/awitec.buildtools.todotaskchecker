﻿using Awitec.BuildTools.TodoTaskChecker.Model;

namespace Awitec.BuildTools.TodoTaskChecker.Helpers
{
    internal interface ILineParser
    {
        LineInfo ParseLine(string line);
    }
}