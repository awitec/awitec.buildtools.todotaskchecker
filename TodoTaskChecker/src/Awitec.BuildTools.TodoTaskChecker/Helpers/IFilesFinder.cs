﻿using System.Collections.Generic;
using Awitec.BuildTools.TodoTaskChecker.Model;

namespace Awitec.BuildTools.TodoTaskChecker.Helpers
{
    public interface IFilesFinder
    {
        IEnumerable<SourceFile> SearchForFiles(string folder, string extension);
    }
}