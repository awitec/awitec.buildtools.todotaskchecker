﻿namespace Awitec.BuildTools.TodoTaskChecker.Model
{
    public class LineInfo
    {
        public LineInfo(bool containsTodo, string taskId)
        {
            ContainsTodo = containsTodo;
            TaskId = taskId;
        }

        public bool ContainsTodo { get; }

        public string TaskId { get; }

        public static LineInfo Default => new LineInfo(false, string.Empty);
    }
}