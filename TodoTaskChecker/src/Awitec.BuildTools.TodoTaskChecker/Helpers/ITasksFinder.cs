﻿using System.Collections.Generic;
using Awitec.BuildTools.TodoTaskChecker.Model;

namespace Awitec.BuildTools.TodoTaskChecker.Helpers
{
    public interface ITasksFinder
    {
        IEnumerable<WorkTask> GetWorkItems(string project, string serverUri, string token);
    }
}