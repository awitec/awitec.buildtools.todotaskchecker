﻿namespace Awitec.BuildTools.TodoTaskChecker.Model
{
    public class ToDo
    {
        public string FileName { get; set; }

        public int LineNumber { get; set; }

        public string LineContent { get; set; }

        public string TaskNumber { get; set; }
    }
}
