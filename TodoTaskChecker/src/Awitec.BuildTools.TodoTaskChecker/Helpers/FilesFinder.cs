﻿using System.Collections.Generic;
using System.IO;
using Awitec.BuildTools.TodoTaskChecker.Model;

namespace Awitec.BuildTools.TodoTaskChecker.Helpers
{
    internal class FilesFinder : IFilesFinder
    {
        public IEnumerable<SourceFile> SearchForFiles(string folder, string extension)
        {
            if (!Directory.Exists(folder))
            {
                throw new DirectoryNotFoundException(folder);
            }

            var searchPattern = string.IsNullOrWhiteSpace(extension)
                ? "*.*"
                : $"*.{extension.Trim('.')}";

            foreach (var filePath in Directory.GetFiles(folder, searchPattern, SearchOption.AllDirectories))
            {
                yield return GetFileFromPath(filePath);
            }
        }

        private static SourceFile GetFileFromPath(string path)
        {
            return new SourceFile
            {
                FullPath = Path.GetFullPath(path),
                Extension = Path.GetExtension(path),
                FullName = Path.GetFileName(path)
            };
        }
    }
}
