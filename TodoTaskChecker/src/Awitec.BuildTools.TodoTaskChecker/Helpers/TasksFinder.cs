﻿using System;
using System.Linq;
using System.Collections.Generic;
using Awitec.BuildTools.TodoTaskChecker.Model;
using Microsoft.TeamFoundation.WorkItemTracking.WebApi;
using Microsoft.TeamFoundation.WorkItemTracking.WebApi.Models;
using Microsoft.VisualStudio.Services.Common;

namespace Awitec.BuildTools.TodoTaskChecker.Helpers
{
    internal class TasksFinder : ITasksFinder
    {
        private const string TaskFieldId = "System.Id";
        private const string TaskFieldTitle = "System.Title";
        private const string TaskFieldState = "System.State";

        public IEnumerable<WorkTask> GetWorkItems(string project, string serverUri, string token)
        {
            var uri = new Uri(serverUri);
            var credentials = new VssBasicCredential(string.Empty, token);
            var query = new Wiql
            {
                Query = "Select [State], [Title] " +
                        "From WorkItems " +
                        "Where [Work Item Type] = 'Task' " +
                        "And [System.TeamProject] = '" + project + "' " +
                        "And [System.State] <> 'Closed' " +
                        "Order By [State] Asc, [Changed Date] Desc"
            };

            using (var workItemTrackingHttpClient = new WorkItemTrackingHttpClient(uri, credentials))
            {
                var workItemQueryResult = workItemTrackingHttpClient.QueryByWiqlAsync(query).Result;

                if (!workItemQueryResult.WorkItems.Any())
                {
                    return Enumerable.Empty<WorkTask>();
                }

                var ids = workItemQueryResult.WorkItems.Select(s => s.Id).ToArray();
                var fields = new [] { TaskFieldId,  TaskFieldTitle, TaskFieldState } ;
                var workItems = workItemTrackingHttpClient.GetWorkItemsAsync(ids, fields, workItemQueryResult.AsOf).Result;

                return workItems.Select(ToWorkItem);
            }
        }

        private static WorkTask ToWorkItem(WorkItem azureWorkItem)
        {
            return new WorkTask
            {
                Id = azureWorkItem.Fields[TaskFieldId].ToString(),
                Name = azureWorkItem.Fields[TaskFieldTitle].ToString(),
                Status = azureWorkItem.Fields[TaskFieldState].ToString()
            };
        }
    }
}
