﻿using System;
using System.Linq;
using Awitec.BuildTools.TodoTaskChecker.Helpers;
using Awitec.BuildTools.TodoTaskChecker.Model;
using Moq;
using Shouldly;
using Xunit;

namespace Awitec.BuildTools.TodoTaskChecker.Tests
{
    public class TodoCheckerTests
    {
        [Theory]
        [InlineData("")]
        [InlineData("\t")]
        [InlineData(null)]
        public void DoAllToDosHaveOpenTask_ServerUrlIsNullEmptyWhiteSpace_ThrowsArgumentNullException(string serverUrl)
        {
            var checker = new TodoCheckerTestsContext().GetNewToDoChecker();

            Should.Throw<ArgumentNullException>(() => checker.DoAllToDosHaveOpenTask(serverUrl, "project", "token", "dir"));
        }

        [Theory]
        [InlineData("")]
        [InlineData("\t")]
        [InlineData(null)]
        public void DoAllToDosHaveOpenTask_ProjectNameIsNullEmptyWhiteSpace_ThrowsArgumentNullException(string project)
        {
            var checker = new TodoCheckerTestsContext().GetNewToDoChecker();

            Should.Throw<ArgumentNullException>(() => checker.DoAllToDosHaveOpenTask("server", project, "token", "dir"));
        }

        [Theory]
        [InlineData("")]
        [InlineData("\t")]
        [InlineData(null)]
        public void DoAllToDosHaveOpenTask_AccessTokenIsNullEmptyWhiteSpace_ThrowsArgumentNullException(string token)
        {
            var checker = new TodoCheckerTestsContext().GetNewToDoChecker();

            Should.Throw<ArgumentNullException>(() => checker.DoAllToDosHaveOpenTask("server", "project", token, "dir"));
        }

        [Theory]
        [InlineData("")]
        [InlineData("\t")]
        [InlineData(null)]
        public void DoAllToDosHaveOpenTask_WorkingDirectoryIsNullEmptyWhiteSpace_ThrowsArgumentNullException(string dir)
        {
            var checker = new TodoCheckerTestsContext().GetNewToDoChecker();

            Should.Throw<ArgumentNullException>(() => checker.DoAllToDosHaveOpenTask("server", "project", "token", dir));
        }

        [Fact]
        public void DoAllToDosHaveOpenTask_NoFilesInDirectory_ReturnsTrue()
        {
            var context = new TodoCheckerTestsContext();
            var checker = context.GetNewToDoChecker();

            context.FileFinder
                .Setup(s => s.SearchForFiles(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Enumerable.Empty<SourceFile>)
                .Verifiable();

            var result = checker.DoAllToDosHaveOpenTask("url", "project", "token", "dir");

            result.ShouldBeTrue();
            context.FileFinder.Verify();
        }

        [Fact]
        public void DoAllToDosHaveOpenTask_NoToDosInFile_ReturnsTrue()
        {
            var context = new TodoCheckerTestsContext();
            var checker = context.GetNewToDoChecker();
            var sourceFile = new SourceFile { Extension = "cs", FullName = "Test.cs", FullPath = "~/Test.cs" };

            context.FileFinder
                .Setup(s => s.SearchForFiles(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new[] { sourceFile })
                .Verifiable();

            context.ToDosFinder
                .Setup(s => s.SearchForToDos(It.IsAny<SourceFile>()))
                .Returns(Enumerable.Empty<ToDo>)
                .Verifiable();

            var result = checker.DoAllToDosHaveOpenTask("url", "project", "token", "dir");

            result.ShouldBeTrue();
            context.FileFinder.Verify();
            context.ToDosFinder.Verify();
        }

        [Fact]
        public void DoAllToDosHaveOpenTask_NonTrackedToDosFind_ReturnsFalse()
        {
            var context = new TodoCheckerTestsContext();
            var checker = context.GetNewToDoChecker();
            var sourceFile = new SourceFile { Extension = "cs", FullName = "Test.cs", FullPath = "~/Test.cs" };
            var todo = new ToDo { FileName = "Test.cs", LineContent = "TODO: 123", LineNumber = 20, TaskNumber = "123" };

            context.FileFinder
                .Setup(s => s.SearchForFiles(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new [] { sourceFile })
                .Verifiable();

            context.ToDosFinder
                .Setup(s => s.SearchForToDos(It.IsAny<SourceFile>()))
                .Returns(new[] { todo })
                .Verifiable();

            var result = checker.DoAllToDosHaveOpenTask("url", "project", "token", "dir");

            result.ShouldBeFalse();
            context.FileFinder.Verify();
            context.ToDosFinder.Verify();
        }

        [Fact]
        public void DoAllToDosHaveOpenTask_TrackedToDosFind_ReturnsTrue()
        {
            var context = new TodoCheckerTestsContext();
            var checker = context.GetNewToDoChecker();
            var sourceFile = new SourceFile { Extension = "cs", FullName = "Test.cs", FullPath = "~/Test.cs" };
            var todo = new ToDo { FileName = "Test.cs", LineContent = "TODO: 123", LineNumber = 20, TaskNumber = "123" };
            var task = new WorkTask { Id = "123", Name = "Finish it", Status = "Active" };

            context.TasksFinder
                .Setup(s => s.GetWorkItems(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new[] { task })
                .Verifiable();

            context.FileFinder
                .Setup(s => s.SearchForFiles(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new[] { sourceFile })
                .Verifiable();

            context.ToDosFinder
                .Setup(s => s.SearchForToDos(It.IsAny<SourceFile>()))
                .Returns(new[] { todo })
                .Verifiable();

            var result = checker.DoAllToDosHaveOpenTask("url", "project", "token", "dir");

            result.ShouldBeTrue();
            context.FileFinder.Verify();
            context.ToDosFinder.Verify();
            context.TasksFinder.Verify();
        }

        [Fact]
        public void TodoChecker_FileFinderIsNull_ThrowsArgumentNullException()
        {
            var context = new TodoCheckerTestsContext
            {
                FileFinder = null
            };

            Should.Throw<ArgumentNullException>(() => context.GetNewToDoChecker());
        }

        [Fact]
        public void TodoChecker_TasksFinderIsNull_ThrowsArgumentNullException()
        {
            var context = new TodoCheckerTestsContext
            {
                TasksFinder = null
            };

            Should.Throw<ArgumentNullException>(() => context.GetNewToDoChecker());
        }

        [Fact]
        public void TodoChecker_ToDosFinderIsNull_ThrowsArgumentNullException()
        {
            var context = new TodoCheckerTestsContext
            {
                ToDosFinder = null
            };

            Should.Throw<ArgumentNullException>(() => context.GetNewToDoChecker());
        }

        private class TodoCheckerTestsContext
        {
            internal Mock<IFilesFinder> FileFinder { get; set; } = new Mock<IFilesFinder>();

            internal Mock<ITasksFinder> TasksFinder { get; set; } = new Mock<ITasksFinder>();

            internal Mock<IToDosFinder> ToDosFinder { get; set; } = new Mock<IToDosFinder>();

            internal TodoChecker GetNewToDoChecker()
            {
                return new TodoChecker(FileFinder?.Object, TasksFinder?.Object, ToDosFinder?.Object);
            }
        }
    }
}
