﻿namespace Awitec.BuildTools.TodoTaskChecker.Model
{
    public class SourceFile
    {
        public string FullName { get; set; }

        public string Extension { get; set; }

        public string FullPath { get; set; }
    }
}
